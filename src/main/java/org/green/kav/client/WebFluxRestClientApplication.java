package org.green.kav.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebFluxRestClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebFluxRestClientApplication.class, args);
	}

}
