package org.green.kav.client.impl.ipf.rest;

import lombok.extern.slf4j.Slf4j;
import org.green.kav.client.impl.ipf.config.IpfConfig;
import org.green.kav.client.model.ipf.InvestigationModel;
import org.green.kav.client.model.ipf.ObservationModel;
import org.green.kav.client.util.ShutdownManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Slf4j
@Component
public class IpfProximtityClient {

    private WebClient.Builder ipfBaseWebClientBuilder;

    private ShutdownManager shutdownManager;

    private IpfConfig ipfConfig;

    @Autowired
    public IpfProximtityClient(WebClient.Builder ipfBaseWebClientBuilder, ShutdownManager shutdownManager, IpfConfig ipfConfig) {
        this.ipfBaseWebClientBuilder = ipfBaseWebClientBuilder;
        this.shutdownManager = shutdownManager;
        this.ipfConfig = ipfConfig;
    }

    public static String readFromFile(String fileName) throws Exception {
        InputStream is = null;
        String fileAsString = null;
        try {
            File file = ResourceUtils.getFile("classpath:" + fileName);
            is = new FileInputStream(file);

            BufferedReader buf = new BufferedReader(new InputStreamReader(is));

            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();

            while (line != null) {
                sb.append(line)
                  .append(System.lineSeparator());
                line = buf.readLine();
            }

            fileAsString = sb.toString();
        }
        finally {
            try {
                is.close();
            }
            catch (Exception ex) {
                throw ex;
            }
        }
        return fileAsString;
    }

    private Stream<InvestigationModel> getProximityInvestigation(WebClient.Builder ipfBaseWebClientBuilder) {
        String url = "/ipf/proximityanalysis/investigations?start=0&limit=10";
        Mono<InvestigationModel[]> investigations = ipfBaseWebClientBuilder.build()
                                                                           .get()
                                                                           .uri(url)
                                                                           .accept(MediaType.APPLICATION_JSON)
                                                                           .retrieve()
                                                                           .bodyToMono(InvestigationModel[].class);
        InvestigationModel[] investigationModels = investigations.block();
        log.info("investigations size: {}", investigations.block().length);
        return Stream.of(investigationModels);
    }

    private Flux<String> createNewInvestigations(int investigationsToCreate) {
        final String url = "/ipf/proximityanalysis/investigation/create";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        LocalDateTime localDateTime = null;
        Mono<InvestigationModel> investigationModel;
        InvestigationModel temp;
        List<String> investigationIds = new ArrayList<>(investigationsToCreate);

        for (int i = 0; i < investigationsToCreate; i++) {
            localDateTime = LocalDateTime.now();
            String body = "{\"investigationName\":\"" + localDateTime.format(formatter) + "_" + i + "\"}";
            log.info("Request body: {}", body);
            investigationModel = ipfBaseWebClientBuilder.build()
                                                        .post()
                                                        .uri(url)
                                                        .body(Mono.just(body), String.class)
                                                        .accept(MediaType.APPLICATION_JSON)
                                                        .exchange()
                                                        .flatMap(clientResponse -> clientResponse.bodyToMono(InvestigationModel.class));

            temp = investigationModel.block();
            log.info("Created investigation:[name: {}, id: {}]", temp.getInvestigationName(), temp.getInvestigationId());
            investigationIds.add(temp.getInvestigationId());
        }
        return Flux.fromIterable(investigationIds);
    }

    public void createProximityObservation(String investigationId) {
        String url = "/ipf/proximityanalysis/investigation/" + investigationId + "/observation/create";
        log.info("Creating observation for investigation: {}", investigationId);

        ipfConfig.getIdentityFiles()
                 .forEach(s -> {
                     try {
                         createObservation(url, IpfProximtityClient.readFromFile(s));
                     }
                     catch (Exception e) {
                         e.printStackTrace();
                     }
                 });
    }

    public void createProximityObservation(String investigationId, String identityId) {
        String url = "/ipf/proximityanalysis/investigation/" + investigationId + "/observation/create";
        log.info("Creating observation for investigation: {}", investigationId);

        try {
            createObservation(url, "{" + "\"" + "identityId" + "\"" + ":" + "\"" + identityId + "\"" + "}");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createObservation(String url, String body) {
        Mono<ObservationModel> observationMono = ipfBaseWebClientBuilder.build()
                                                                        .post()
                                                                        .uri(url)
                                                                        .body(Mono.just(body), String.class)
                                                                        .accept(MediaType.APPLICATION_JSON)
                                                                        .retrieve()
                                                                        .bodyToMono(ObservationModel.class);

        log.info("observationId: {}",
                 observationMono.block()
                                .getObservationId());
    }

    public void run(String... args) throws Exception {
        createNewInvestigations(ipfConfig.getInvestigationsToCreate()).subscribe(this::createProximityObservation);

        getProximityInvestigation(ipfBaseWebClientBuilder).filter(investigationModel -> {
            if (investigationModel.getInvestigationStatus()
                                  .equalsIgnoreCase("CREATED")) {
                return true;
            }
            return false;
        }).forEach(investigationModel -> log.info("CREATED: {}", investigationModel.toString()));

        shutdownManager.initiateShutdown(9);
    }
}
