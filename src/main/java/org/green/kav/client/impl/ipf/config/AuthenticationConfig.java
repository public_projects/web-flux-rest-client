package org.green.kav.client.impl.ipf.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "auth.config")
@Getter
@Setter
public class AuthenticationConfig {
    private String username;
    private String password;
    private String instance;
    private String agency;

    @Override
    public String toString() {
        return "AuthenticationConfig{" + "username='" + username + '\'' + ", password='" + password + '\'' + ", instance='" + instance +
               '\'' + ", agency='" + agency + '\'' + '}';
    }
}
