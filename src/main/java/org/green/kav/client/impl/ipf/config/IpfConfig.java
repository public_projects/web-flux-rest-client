package org.green.kav.client.impl.ipf.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;


@Configuration
@ConfigurationProperties(prefix = "ipf")
@Getter
@Setter
public class IpfConfig {
    private String baseUrl;
    private int investigationsToCreate;
    private List<String> identityFiles;
}
