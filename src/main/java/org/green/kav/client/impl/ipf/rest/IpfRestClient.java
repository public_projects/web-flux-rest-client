package org.green.kav.client.impl.ipf.rest;

import org.green.kav.client.impl.ipf.config.IpfConfig;
import org.green.kav.client.util.ShutdownManager;
import org.springframework.web.reactive.function.client.WebClient;

public class IpfRestClient {
    protected WebClient.Builder ipfBaseWebClientBuilder;

    protected ShutdownManager shutdownManager;

    protected IpfConfig ipfConfig;

    public IpfRestClient(WebClient.Builder ipfBaseWebClientBuilder, ShutdownManager shutdownManager, IpfConfig ipfConfig) {
        this.ipfBaseWebClientBuilder = ipfBaseWebClientBuilder;
        this.shutdownManager = shutdownManager;
        this.ipfConfig = ipfConfig;
    }
}
