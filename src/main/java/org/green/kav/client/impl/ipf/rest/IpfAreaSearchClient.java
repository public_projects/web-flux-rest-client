package org.green.kav.client.impl.ipf.rest;

import lombok.extern.slf4j.Slf4j;
import org.green.kav.client.impl.ipf.config.IpfConfig;
import org.green.kav.client.model.ipf.areasearch.AreaSearchInvestigationModel;
import org.green.kav.client.util.ShutdownManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class IpfAreaSearchClient extends IpfRestClient {

    @Autowired
    public IpfAreaSearchClient(WebClient.Builder ipfBaseWebClientBuilder, ShutdownManager shutdownManager, IpfConfig ipfConfig) {
        super(ipfBaseWebClientBuilder, shutdownManager, ipfConfig);
    }

    public void getAllAreaSearch() {
        String url = "ipf/query/area-search/investigations?start=0&limit=10";

        Mono<String> dataBlock = ipfBaseWebClientBuilder.build()
                               .get()
                               .uri(url)
                               .accept(MediaType.APPLICATION_JSON)
                               .retrieve().bodyToMono(String.class);

//        Mono<AreaSearchInvestigationModel[]> investigations = ipfBaseWebClientBuilder.build()
//                                                                                     .get()
//                                                                                     .uri(url)
//                                                                                     .accept(MediaType.APPLICATION_JSON)
//                                                                                     .retrieve()
//                                                                                     .bodyToMono(AreaSearchInvestigationModel[].class);
//        AreaSearchInvestigationModel[] investigationModels = investigations.block();
//        log.info("investigations size: {}", investigations.block().length);

        System.err.println(dataBlock.block());
//        return Stream.of(investigationModels);
    }
}
