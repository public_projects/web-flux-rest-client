package org.green.kav.client.impl.ipf;

import lombok.extern.slf4j.Slf4j;
import org.green.kav.client.impl.ipf.config.AuthenticationConfig;
import org.green.kav.client.impl.ipf.config.IpfConfig;
import org.green.kav.client.model.ipf.AuthenticationRequestModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Configuration
@Slf4j
public class IpfApiAuthentication {
    public static final String TOKEN_NAME = "X-Security-Token";
    // TODO implement profile

    private IpfConfig ipfConfig;

    @Autowired
    public IpfApiAuthentication(IpfConfig ipfConfig){
        this.ipfConfig = ipfConfig;
    }

    @Bean(name = "ipfBaseWebClientBuilder")
    public WebClient.Builder getIpfBaseWebClientBuilder(AuthenticationConfig authenticationConfig) {
        WebClient.Builder builder = WebClient.builder()
                                             .baseUrl(ipfConfig.getBaseUrl());

        Map<String, String[]> tokenKey = authenticate(builder, authenticationConfig);

        return builder.defaultHeader(TOKEN_NAME, tokenKey.get(TOKEN_NAME))
                      .defaultHeader("Content-Type", "application/json");
    }

    private Map<String, String[]> authenticate(WebClient.Builder ipfBaseWebClientBuilder, AuthenticationConfig authenticationConfig) {

        AuthenticationRequestModel authenticationRequestModel = new AuthenticationRequestModel(authenticationConfig.getUsername(),
                                                                                               authenticationConfig.getPassword(),
                                                                                               authenticationConfig.getInstance(),
                                                                                               authenticationConfig.getAgency());

        String url = "/ipf/security/authenticate";

        final Map<String, List<String>> token = new HashMap<>();
        Mono<Object> objectFlux = ipfBaseWebClientBuilder.build()
                                                         .post()
                                                         .uri(url)
                                                         .body(Mono.just(authenticationRequestModel), AuthenticationRequestModel.class)
                                                         .accept(MediaType.APPLICATION_JSON)
                                                         .exchange()
                                                         .doOnSuccess(clientResponse -> token.put(TOKEN_NAME,
                                                                                                  clientResponse.headers()
                                                                                                                .header(TOKEN_NAME)))
                                                         .flatMap(clientResponse -> clientResponse.bodyToMono(String.class));

        log.debug("Output: {}", objectFlux.block());

        String[] key = new String[1];
        key = token.get(TOKEN_NAME)
                   .toArray(key);

        Map<String, String[]> tokenHeader = new HashMap<>();
        tokenHeader.put(TOKEN_NAME, key);
        log.info("token: {}", token);

        return tokenHeader;
    }
}
