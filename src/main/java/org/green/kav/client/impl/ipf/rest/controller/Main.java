package org.green.kav.client.impl.ipf.rest.controller;

import org.green.kav.client.impl.ipf.rest.IpfAreaSearchClient;
import org.green.kav.client.impl.ipf.rest.IpfProximtityClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Main implements CommandLineRunner {

    @Autowired
    private IpfAreaSearchClient ipfAreaSearchClient;

    @Autowired
    private IpfProximtityClient ipfProximtityClient;

    @Override
    public void run(String... args) throws Exception {
//        ipfAreaSearchClient.getAllAreaSearch();
//        ipfProximtityClient.run();
    }
}
