package org.green.kav.client.model.ipf.areasearch;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Properties {
    public String instance;
    @JsonProperty("agency-id")
    public String agencyId;
    @JsonProperty("agency-name")
    public String agencyName;
}
