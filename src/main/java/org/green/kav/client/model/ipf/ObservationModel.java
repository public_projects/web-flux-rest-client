package org.green.kav.client.model.ipf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class ObservationModel {
    List<ObservationIdentityModel> observedIdentities = new ArrayList<>();
    private String observationId;

    @Getter
    @Setter
    @NoArgsConstructor
    static class ObservationIdentityModel {
        private String identityId;
        private String identityCaption;
        private String identityNature;
        private String serviceName;
    }
}
