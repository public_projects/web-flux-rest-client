package org.green.kav.client.model.ipf;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
public class AuthenticationRequestModel implements Serializable {
    @JsonProperty("@type")
    private String type = "com.ipfli.ipf.security.web.api.shared.UserCredentials";
    private final String username;
    private final String password;
    private final String instance;
    private final String agency;
    public AuthenticationRequestModel(String username, String password, String instance, String agency) {
        this.username = username;
        this.password = password;
        this.instance = instance;
        this.agency = agency;
    }
}
