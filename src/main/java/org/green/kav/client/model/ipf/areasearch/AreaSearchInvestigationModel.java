package org.green.kav.client.model.ipf.areasearch;

import java.util.List;

public class AreaSearchInvestigationModel {
    public int participantsCount;
    public int coordinatorsCount;
    public long validFrom;
    public long validTo;
    public List<Source> sources;
    public String user;
    public String role;
    public String description;
    public Source source;
    public List<CellSpec> cellSpecs;
    public List<String> geoHashes;
    public String id;
}
