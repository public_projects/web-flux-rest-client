package org.green.kav.client.model.ipf.areasearch;

public class CellMetadata{
    public double rightLatitude;
    public double rightLongitude;
    public double leftLatitude;
    public double leftLongitude;
    public double midLatitude;
    public double midLongitude;
    public double azimuth;
    public double height;
    public double coverageRadius;
    public double mechanicalDowntilt;
    public double btsTransmitterPower;
    public String towerName;
    public String cellName;
    public double latitude;
    public double longitude;
    public Object insertTimestamp;
    public Object usageTimestamp;
    public Object actualCgi;
}
