package org.green.kav.client.model.ipf.areasearch;

public class CellSpec {
    public int mcc;
    public int mnc;
    public int lac;
    public int cellId;
    public double latitude;
    public double longitude;
    public String cgi;
    public CellMetadata cellMetadata;
    public LocationMetadata locationMetadata;
}
