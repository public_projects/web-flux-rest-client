package org.green.kav.client.model.ipf;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class InvestigationModel implements Serializable {
    private String investigationId;
    private String investigationName;
    private String investigationStatus;

    @Override
    public String toString() {
        return "InvestigationModel{" + "investigationId='" + investigationId + '\'' + ", investigationName='" + investigationName + '\'' +
               ", investigationStatus='" + investigationStatus + '\'' + '}';
    }
}
