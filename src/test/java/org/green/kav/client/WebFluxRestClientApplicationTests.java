package org.green.kav.client;

import org.green.kav.client.impl.ipf.rest.IpfProximtityClient;
import org.javatuples.Pair;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SpringBootTest
class WebFluxRestClientApplicationTests {

	@Autowired
	private IpfProximtityClient ipfProximtityClient;
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final DateTimeFormatter logDateFormat =
			DateTimeFormatter.ofPattern(DATE_FORMAT).withZone(ZoneId.systemDefault());

	@Disabled
	@Test
	void hash(){
		String d1 = "006Smsisdn015S587216674334764";
		String d2 = "006Smsisdn015S600856483322156";
		String d3 = "006Smsisdn015S601778506650613";

		StringBuilder sb1 = new StringBuilder();
		sb1.append(d1).append(d2).append(d3);

		System.err.println("1: " + sb1.hashCode());
		System.err.println("2: " + (d1+d2+d3).hashCode());
		System.err.println("2.1: " + (d2+d3+d1).hashCode());

		StringBuilder sb2 = new StringBuilder();
		sb2.append(d3).append(d2).append(d1);

		System.err.println("3: " + sb2.hashCode());

		StringBuilder sb3 = new StringBuilder();
		sb3.append(d3).append(d1).append(d2);

		System.err.println("4: " + sb3.hashCode());
	}

	@Test
	public void testBiggerSmaller(){

		String range = "1596615002000_1599293101000";

		Long value0 = Long.parseLong(range.substring(0, range.indexOf("_")));
		Long value1 = Long.parseLong(range.substring(range.indexOf("_") + 1, range.length()));

		System.err.println(logDateFormat.format(Instant.ofEpochMilli(value0)));
		System.err.println(logDateFormat.format(Instant.ofEpochMilli(value1)));
		Long startRange = 1628402504000l; // Sunday, August 8, 2021 6:01:44
		Long baseTime = 1633672904000l;
		Long endRange = 1633672904000l; //Friday, October 8, 2021 6:01:44
		Long nextRange = 1633672904090l;

		if (startRange <= baseTime && baseTime < endRange) {
			// Keep adding to the result
			String rangeKey = startRange + "_" + endRange;
			System.err.println(rangeKey);
		}

		if (endRange <= baseTime && baseTime < nextRange) {
			// Keep adding to the result
			String rangeKey = startRange + "_ | _" + endRange;
			System.err.println(rangeKey);
		}
	}

	private List<Pair<Long, Long>> getMaxGapBuckets(Set<Long> baseTimes, Long maxGap) {
		Long lowerEnd = Collections.min(baseTimes);
		Long maxTime = Collections.max(baseTimes);
		System.err.println("earliest: " + logDateFormat.format(Instant.ofEpochMilli(lowerEnd)));
		System.err.println("max: " + logDateFormat.format(Instant.ofEpochMilli(maxTime)));

		// List<StartTime, EndTime>
		List<Pair<Long, Long>> maxGapTimeBucket = new ArrayList<>();

		Long newTail = 0l;
		while (newTail < maxTime) {
			newTail = lowerEnd + maxGap;
			if (newTail > maxTime) {
				maxGapTimeBucket.add(new Pair<>(lowerEnd+1, maxTime));
				break;
			}
			Pair<Long, Long> timeBucket = new Pair<>(lowerEnd+1, newTail);
			maxGapTimeBucket.add(timeBucket);
			lowerEnd = newTail;
		}

		if (!maxGapTimeBucket.isEmpty()) {
			Pair<Long, Long> first = maxGapTimeBucket.get(0);
			Pair<Long, Long> newFirst = first.setAt0(first.getValue0() - 1);
			maxGapTimeBucket.set(0, newFirst);
		}
		return maxGapTimeBucket;
	}

	@Test
	public void whoIsBig(){
		Long startTime = System.currentTimeMillis();
		Long ori = 1628402504000l; // Sunday, August 8, 2021 6:01:44
		Long next = 1633672904000l; //Friday, October 8, 2021 6:01:44
		Set<Long> bunchOfTimestamp = new HashSet<>();
		bunchOfTimestamp.add(ori);
		bunchOfTimestamp.add(next);

		List<Pair<Long, Long>> maxGapTimeBucket = getMaxGapBuckets(bunchOfTimestamp, 600001l);

		for (Pair<Long, Long> bucket: maxGapTimeBucket) {
			String start = logDateFormat.format(Instant.ofEpochMilli(bucket.getValue0()));
			String end = logDateFormat.format(Instant.ofEpochMilli(bucket.getValue1()));
			System.err.println(start + ", " + end);
		}

		System.err.println(System.currentTimeMillis() - startTime);
	}

//	@Test
//	void createObservations() {
//		// Albina Wolf", "Eunice Feest", "Idella Auer", "Clinton Senger", "Akeem Flatley", "Jack Napier", "Cesar Turner", "Francesca Hane", "Adelbert Toy", "Dereck Boyer", "Harleen Frances", "Rosemary Hermiston", "Harold Jordan", "Juan Sloan", "Kaleb Bins","Cindy Hartmann","Leila Mitchell","Jessica Cruz","Mary Jane","Vanessa Altenwerth"
//
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S587216674334764"); // Cindy Hartmann
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S601778506650613"); // Adelbert Toy
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S358851753582748"); // Rosemary Hermiston
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S615734501717247"); // Idella Auer
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S852368205040446"); // Albina Wolf
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn012S974060569534"); // Juan Sloan
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S606181422050610"); // Jessica Cruz
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S607240733617155"); // Jack Napier
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S601712621164604"); // Harleen Frances
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S599430531221552"); // Kaleb Bins
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S387006400627163"); // Vanessa Altenwerth
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S361817357103352"); // Eunice Feest
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S244502168316128"); // Dereck Boyer
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S600630105235468"); // Francesca Hane
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S525348618674583"); // Cesar Turner
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S601528351633212"); // Harold Jordan
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S506260031620872"); // Leila Mitchell
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S134578555538527"); // Clinton Senger
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S600856483322156"); // Mary Jane
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Smsisdn015S244150537430722"); // Akeem Flatley
//		ipfProximtityClient.createProximityObservation("2f8eef40-1eaf-11ed-9b40-2c0da7f630d4", "006Sonline015Sakeem@skype.com005Sskype"); // Akeem Flatley
//	}

	@Test
	public void testTuple(){
		Pair<Long, List<String>> group = new Pair<>(123L, Arrays.asList("1", "2", "3"));
		Map<String, Pair<Long, List<String>>> newRawCollection = new HashMap<>();
		newRawCollection.put("123", group);
		List<String> keys = new ArrayList<>();
		keys.add("006smsisdn016s8759026200805003");
		keys.add("006smsisdn016s8759026200805004");
		keys.add("006smsisdn016s8759026200805005");
		keys.add("006smsisdn016s8759026200805006");
		keys.add("006smsisdn016s8759026200805002");
		Collections.sort(keys);

		System.err.println(String.join("#", keys));
	}

	@Disabled
	@Test
	public void createObservations(){
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S587216674334764"); // Cindy Hartmann
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S601778506650613"); // Adelbert Toy
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S358851753582748"); // Rosemary Hermiston
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S615734501717247"); // Idella Auer
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S852368205040446"); // Albina Wolf
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn012S974060569534"); // Juan Sloan
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S606181422050610"); // Jessica Cruz
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S607240733617155"); // Jack Napier
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S601712621164604"); // Harleen Frances
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S599430531221552"); // Kaleb Bins
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S387006400627163"); // Vanessa Altenwerth
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S361817357103352"); // Eunice Feest
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S244502168316128"); // Dereck Boyer
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S600630105235468"); // Francesca Hane
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S525348618674583"); // Cesar Turner
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S601528351633212"); // Harold Jordan
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S506260031620872"); // Leila Mitchell
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S134578555538527"); // Clinton Senger
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S600856483322156"); // Mary Jane
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Smsisdn015S244150537430722"); // Akeem Flatley
		ipfProximtityClient.createProximityObservation("3435b660-3eed-11ed-80b7-2c0da7f630d4", "006Sonline015Sakeem@skype.com005Sskype"); // Akeem Flatley
	}

//	@Test
//	void createObservations() {
//		ipfProximtityClient.createProximityObservation("f11c47e0-21f5-11ed-a4d3-2c0da7f630d4", "006Smsisdn015S587216674334764"); // Cindy Hartmann
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn015S358851753582748"); // Rosemary Hermiston
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn015S615734501717247"); // Idella Auer
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn015S852368205040446"); // Albina Wolf
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn015S606181422050610"); // Jessica Cruz
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn012S974060569534"); // Juan Sloan
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn015S607240733617155"); // Jack Napier
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn015S601712621164604"); // Harleen Frances
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn015S599430531221552"); // Kaleb Bins
//		ipfProximtityClient.createProximityObservation("8d4dba70-215d-11ed-b4d2-2c0da7f630d4", "006Smsisdn015S601778506650613"); // Adelbert Toy
//	}

//	@Test
//	void createObservations() {
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S358851753582748"); // Rosemary Hermiston
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S615734501717247"); // Idella Auer
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S852368205040446"); // Albina Wolf
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S606181422050610"); // Jessica Cruz
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn012S974060569534"); // Juan Sloan
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S607240733617155"); // Jack Napier
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S601712621164604"); // Harleen Frances
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S599430531221552"); // Kaleb Bins
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S601778506650613"); // Adelbert Toy
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S244150537430722"); // Akeem Flatley
//		ipfProximtityClient.createProximityObservation("b68e6ac0-292c-11ed-9b9a-2c0da7f630d4", "006Smsisdn015S600856483322156"); // Mary Jane
//	}

	@Test
	public void convertTime(){
		Long time =
	}
}
